# https://github.com/thiagopradi/octopus/issues/307
# Using a percona adapter during migrations with a octopus proxy
if defined?(Octopus) && Octopus.enabled?
  base_config = ActiveRecord::Base.connection.config
  base_config.merge!(adapter: 'percona') if ARGV.include?('db:migrate') || ARGV.include?('db:rollback')
  Octopus.config[Rails.env.to_s]['master'] = base_config
  ActiveRecord::Base.connection.initialize_shards(Octopus.config)
end

PerconaMigrator.configure do |config|
  config.tmp_path = File.dirname(Rails.application.paths['log'].first)
  config.global_percona_args = '--recursion-method=none'
end
